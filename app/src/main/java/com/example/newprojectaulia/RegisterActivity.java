package com.example.newprojectaulia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {

    TextView txtRegister;
    EditText edtName,edtPass,edtAddress,edtEmail,edtPhone;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        txtRegister = findViewById(R.id.text_3);
        edtName = findViewById(R.id.edt_uli3);
        edtPass = findViewById(R.id.edt_uli4);
        edtAddress = findViewById(R.id.edt_uli6);
        edtEmail = findViewById(R.id.edt_uli5);
        edtPhone = findViewById(R.id.edt_uli7);
        btnRegister = findViewById(R.id.btn_bila2);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
                startActivity(intent);

            }
        });
    }
}
